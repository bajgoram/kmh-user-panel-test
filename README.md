# [Vue Material Dashboard](https://demos.creative-tim.com/vue-material-dashboard) 

## Quick start

## Clone the repo: `git clone git clone https://bajgoram@bitbucket.org/bajgoram/kmh-user-panel-test.git`.

### install dependencies
`npm install`
### serve with hot reload at localhost:8080
`npm run dev`

KM/H test exercise
├── README.md
├── babel.config.js
├── package.json
├── postcss.config.js
├── public
│   └── index.html
└── src
    ├── App.vue
    ├── assets
    │   ├── img
    │   └── scss
    │       ├── material-dashboard.scss
    │       └── md
    ├── components
    │   ├── SidebarPlugin
    │   │   ├── SideBar.vue
    │   │   ├── SidebarLink.vue
    │   │   └── index.js
    │   ├── Tables
    │   │   ├── RightsTable.vue
    │   │   ├── RolesTable.vue
    │   │   └── UsersTable.vue
    │   └── index.js
    ├── main.js
    ├── material-dashboard.js
    ├── pages
    │   ├── Layout
    │   │   ├── Content.vue
    │   │   └── DashboardLayout.vue
    │   ├── Role
    │   │   └── EditRoleForm.vue
    │   ├── UserProfile
    │   │   └── EditProfileForm.vue
    │   ├── index.js
    │   ├── Rights.js
    │   ├── Roles.js
    │   └── UserProfile.vue
    ├── routes
    │    └── routes.js
    └── store
         ├── index.js
         └── modules
             ├── right.module.js
             ├── role.module.js
             └── user.module.js

```