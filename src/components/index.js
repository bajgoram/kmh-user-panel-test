

// Tables
import UsersTable from "./Tables/UsersTable.vue";
import RolesTable from "./Tables/RolesTable.vue";
import RightsTable from "./Tables/RightsTable.vue";

export {
  UsersTable,
  RolesTable,
  RightsTable
};
