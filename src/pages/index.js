
// Forms
import EditProfileForm from "../pages/UserProfile/EditProfileForm.vue";
import EditRoleForm from "../pages/Role/EditRoleForm.vue";

export { EditProfileForm };
export { EditRoleForm };
