import DashboardLayout from "@/pages/Layout/DashboardLayout.vue";

import UserList from "@/pages/UserList.vue";
import { EditProfileForm } from "@/pages";
import { EditRoleForm } from "@/pages";
import Roles from "@/pages/Roles.vue";
import Rights from "@/pages/Rights.vue";

const routes = [
  {
    path: "/",
    component: DashboardLayout,
    redirect: "/user",
    children: [
      
      {
        path: "user",
        name: "User List",
        component: UserList
      },
      {
        path: "roles",
        name: "Roles",
        component: Roles
      },
      {
        path: "rights",
        name: "Rights",
        component: Rights
      },
      {
        path: "userProfile/:isEdit/:userId",
        name: "User Profile",
        component: EditProfileForm, 
        props: true 
      },
      {
        path: "roleForm/:isEdit/:roleId",
        name: "Role Editing",
        component: EditRoleForm, 
        props: true
      }
    ]
  }
];

export default routes;
