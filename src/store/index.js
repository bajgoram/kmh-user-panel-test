import Vue from 'vue'
import Vuex from 'vuex'

//modules
import { userModule } from './modules/user.module'
import { roleModule } from './modules/role.module'
import { rightModule } from './modules/right.module'

Vue.use(Vuex);

export default new  Vuex.Store({
  state: {},
  mutations: {},
  getters: {},
  actions: {},
  modules: {
    userModule,
    roleModule,
    rightModule
  }
});
