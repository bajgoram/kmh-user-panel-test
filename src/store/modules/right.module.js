export const rightModule = {
    state: {
        rights: [
            {
              id: 1,
              name: "Read"
            },
            {
              id: 2,
              name: "Create"
            },
            {
              id: 3,
              name: "Edit"
            },
            {
              id: 4,
              name: "Delete"
            }
          ]      
    },
    getters: {
        getRights: (state) => {
            return state.rights;
        }
    },
    mutations: {
        addRight(state, name){
            let right = {
              id: state.rights[state.rights.length - 1].id + 1,
              name: name
            }
            state.rights = [
              ...state.rights,
              right
            ]
        },
        deleteRight(state, id){
            state.rights  =  state.rights.filter(item => item.id != id)
        }
    },
    actions: {
      addRightAction (context, right) {
        context.commit('addRight', right.name);
      },
      deleteRightAction (context, id) {
        context.commit('deleteRight', id);
      }
    }
}