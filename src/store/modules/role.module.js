export const roleModule = {
    state: { 
        roles: [
            {
            id: 1,
            name: "Admin",
            rights: [1]
            },
            {
            id: 2,
            name: "Tech Admin",
            rights: [1,2,3]
            },
            {
            id: 3,
            name: "Supervisor",
            rights: [3]
            },
            {
            id: 4,
            name: "Editor",
            rights: [2]
            },
            {
            id: 5,
            name: "Simple",
            rights: [2,3]
            }
        ]
   },
    getters: {
        getRole: (state) => (id) => {
            return state.roles.filter(item => item.id == id)[0];
        },
        getRoles: (state) => {
            return state.roles;
        }
    },
    mutations: {
        addRole(state, role){
            role.id = state.roles[state.roles.length - 1].id + 1;
            state.roles = [
              ...state.roles,
              role
            ]
          },
          updateRole(state, role){
            state.roles = [
              ...state.roles.filter(item => item.id != role.id),
              role
            ]
          },
          deleteRole(state, id){
            state.roles = state.roles.filter(item => item.id != id)
          }
    },
    actions: {
        addRoleAction (context, role) {
            context.commit('addRole', role);
        },
        updateRoleAction (context, role) {
            context.commit('updateRole', role);
        },
        deleteRoleAction (context, id) {
            context.commit('deleteRole', id);
        }
    }
}