export const userModule = {
    state: {   users: [
        {
          id: 1,
          name: "Philip",
          surname: "Rodriguez",
          email: "prodriguez@mail.com",
          phone: "+49 123 456789",
          roles: [1]
        },
        {
          id: 2,
          name: "Jane",
          surname: "Hooper",
          email: "jhooper@mail.com",
          phone: "+49 123 111111",
          roles: [1,2]
        },
        {
          id: 3,
          name: "John",
          surname: "Limo",
          email: "jlimo@mail.com",
          phone: "+49 123 456789",
          roles: [2]
        },
        {
          id: 4,
          name: "Philip",
          surname: "Muller",
          email: "pmuller@mail.com",
          phone: "+49 113 456789",
          roles: [4]
        }
      ],
     },
    getters: {
        getUser: (state) => (id) => {
          return state.users.filter(item => item.id == id)[0];
        }
    },
    mutations: {
        addUser(state, user){
            user.id = state.users[state.users.length - 1].id + 1;
            state.users = [
              ...state.users,
              user
            ]
          },
          updateUser(state, user){
            state.users = [
              ...state.users.filter(item => item.id != user.id),
              user
            ]
          }
    },
    actions: {
        addUserAction (context, user) {
            context.commit('addUser', user);
          },
          updateUserAction (context, user) {
            context.commit('updateUser', user);
          },
    }
}